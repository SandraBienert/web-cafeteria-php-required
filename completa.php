<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="estilos.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&family=Mr+Dafoe&display=swap" rel="stylesheet">
    
    <title>WEB MODULAR CAFETERIA</title>
</head>
<body>

    <div class="container">
        <header class="row"> <!--LOGO IZQUIERDA-->
            <div class="col-6">
                <img src="img/logo.svg" alt="logo cafeteria taza de cafe" class="logo">
            </div>
            <div class="col-6"> <!--Navbar-->
                <div class="btn-group mt-5 d-flex justify-content-right">
                    <button type="button" class="btn btn-dark">Home</button>
                    <button type="button" class="btn btn-dark">Sobre mí</button>
                    <button type="button" class="btn btn-dark">Servicios</button>
                    <button type="button" class="btn btn-dark">Contacto</button>
                </div>    
            </div>
        </header>
    
    </div>
                <!--AQUI TERMINA CABECERA-->

                <!--COMIENZA INDEX-->

<main class="container"> <!--Parte de la izquierda-->
    <div class="row d-flex">
        <div class="col-8 mt-4">
            <article class="mt-4 ps-4"> <!--ARTICULO 1-->
                <h2 class="titulo">Título del 1r artículo</h2> 
                <p class="mt-4"> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis, aspernatur quisquam quis error sapiente sed dolorem at amet voluptates.</p>
            </article>
            <article class="mt-4 ps-4"> <!--ARTICULO 2-->
                <h2 class="titulo">Título del 2º artículo</h2>
                <p class="mt-4">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis, aspernatur quisquam quis error sapiente sed dolorem at amet voluptates.</p>
            </article>
            <article class="mt-4 ps-4"><!--ARTICULO 3-->
                <h2 class="titulo">Título del 3r artículo</h2>
                <p class="mt-4">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis, aspernatur quisquam quis error sapiente sed dolorem at amet voluptates.</p>
            </article>
        </div>
        <!--FIN DE LA PARTE CENTRAL-->
        
        <!--PARTE DERECHA ASIDE -- FALTA FONDO--> 
        <aside class="col-3 p-4">
            <div>
                <img src="img/cafe5.jpg" alt="foto de una taza con café" class="cafe">
            </div>
            <div class="pt-5">
                <img src="img/cafeteria5.jpg" alt="foto de una terraza de cafetería" class="cafeteria">
            </div>
        </div>
        </aside>
    </div>
</main>
<!--FIN PARTE INDEX-->

<!--FOOTER-->

<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-12"> <!--izquierda barra navegacion-->
                <div class="btn-group mt-4">
                        <button type="button" class="btn btn-dark">Home</button>
                        <button type="button" class="btn btn-dark">Sobre mí</button>
                        <button type="button" class="btn btn-dark">Servicios</button>
                        <button type="button" class="btn btn-dark">Contacto</button>
                </div> 
            </div> 
        </div>
                                <!--Derecha redes sociales-->
        <div class="col-12 in-line d-flex justify-content-end">
            <a href="http://www.facebook.com"><img src="img/social_facebook_fb_35.png" alt="logo de facebook" class="face me-4 mb-4"></a>
            <a href="http://www.twitter.com"><img src="img/social_Twitter_38.png" alt="logo twitter" class="face me-4 mb-4"></a>
            <a href="http://www.linkedin.com"><img src="img/Linke.png" alt="logo linkedin" class="face me-4 mb-4">
        </div>
</div>
</body>
</html>

    
    
    




