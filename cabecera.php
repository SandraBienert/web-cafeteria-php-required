<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <link href="css/estilos3.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@700&family=Mr+Dafoe&display=swap" rel="stylesheet">
    
    <title>WEB MODULAR CAFETERIA</title>
</head>
<body>

    <div class="container-fluid">
        <header id="row" class="row"> <!--LOGO IZQUIERDA-->
            <div class="col-6">
                <img src="img/logo.svg" alt="logo cafeteria taza de cafe" class="logo">
            </div>
            <div id="menu" class="col-6"> <!--Navbar-->
                <div class="btn-group mt-5 d-flex justify-content-right">
                    <button type="button" class="btn btn-dark"><a class="enlace" href="index.php">Home</a></button>
                    <button type="button" class="btn btn-dark"><a class="enlace" href="sobremi.php">Sobre mí</a></button>
                    <button type="button" class="btn btn-dark"><a class="enlace" href="#">Servicios</a></button>
                    <button type="button" class="btn btn-dark"><a class="enlace" href="contacto.php">Contacto</a></button>
                </div>    
            </div>
        </header>
    
    </div>
                <!--AQUI TERMINA CABECERA-->

                