
<style>

*{
    box-sizing: border-box;
    padding: 0;
    margin:0;
    border: 0;
}

.container2{
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
    

    /*width: 100%;
    height: 100%;*/

}

.padre{
    display: flex;
    flex-direction: row;
    justify-content: center;

    
    background-color: #F5E8B7;
    height: 525px;
    width: 100%;
}

h2{
    text-align: left;
    margin: 40px;
    padding-top: 10px;
    

}

.formulario{

    width: 350px;
    height: 400px;
    font-size: 16px;

}

input{

    margin: 10px;
    border: 1px solid black;
    width: 300px;
    
}



label{
    
    padding-left: 20px;
    margin: 10px;
}

textarea{
    border: 1px solid black;
    margin: 10px;
}

.boton{

    border: 1px solid black;
    background-color:brown;
    color: white;
    width: 100px;
    height: 30px;
    margin: 10px;
    margin-left: 100px;
}

/*VALIDACION FORMULARIO*/

</style>

<?php include'cabecera.php'; ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulario web cafeteria</title>
</head>

<body> 

<main class="container2">

<section class="padre">
    
    <h2>Formulario de contacto</2>
    
        <form class="formulario">
            <br>
        <label for="nombre">Nombre: </label>
        <input type="text" name="nombre" id="nombre">
            <br>
        <label for="apellidos">Apellidos: </label>
        <input type="text" name="apellidos" id="apellidos">
            <br>
        <label for="telefono">Teléfono: </label>
        <input type="text" name="telefono" id="telefono">
            <br>
        <label for="correo">Correo:</label>
        <input type="email" name="correo" id="correo">
            <br>
        <label for="comentario">Comentario: </label>
        <textarea rows="5" cols="40" id="comentario" name="comentario"></textarea>
            <br>
        
        <button class="boton" type="button" value="submit" name="enviar" id="submit">ENVIAR</button>
        </form>

    </section>

    <div class="panelderecho">
                <?php require 'panelderecho.php';?>
    </div>

</main>

<?php include 'pie.php'; ?>
</body>
</html>