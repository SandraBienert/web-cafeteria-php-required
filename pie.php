
<style>
footer.footer {
    background-color: #6A9C89;
    height: 120px;
    width: 100%;
}

.row {
    display: flex;
    flex-direction: row;
}

</style>
<!--FOOTER-->

<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12"> <!--izquierda barra navegacion-->
                <div class="btn-group mt-4">
                        <button type="button" class="btn btn-dark">Home</button>
                        <button type="button" class="btn btn-dark">Sobre mí</button>
                        <button type="button" class="btn btn-dark">Servicios</button>
                        <button type="button" class="btn btn-dark">Contacto</button>
                </div> 
            </div> 
        </div>
                                <!--Derecha redes sociales-->
        <div class="col-12 in-line d-flex justify-content-end">
            <a href="http://www.facebook.com"><img src="img/social_facebook_fb_35.png" alt="logo de facebook" class="face me-4 mb-4"></a>
            <a href="http://www.twitter.com"><img src="img/social_Twitter_38.png" alt="logo twitter" class="face me-4 mb-4"></a>
            <a href="http://www.linkedin.com"><img src="img/Linke.png" alt="logo linkedin" class="face me-4 mb-4">
        </div>
</footer>
</body>
</html>
