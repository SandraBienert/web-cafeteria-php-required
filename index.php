
<?php require 'cabecera.php'; ?>

 <!--COMIENZA INDEX-->

<div class="container"> <!--Parte de la izquierda-->
    <div class="row d-flex">
        <div class="col-8 mt-4">
            <article class="mt-4 ps-4"> <!--ARTICULO 1-->
                <h2 class="titulo">Título del 1r artículo</h2> 
                <p class="mt-4"> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis, aspernatur quisquam quis error sapiente sed dolorem at amet voluptates.</p>
            </article>
            <article class="mt-4 ps-4"> <!--ARTICULO 2-->
                <h2 class="titulo">Título del 2º artículo</h2>
                <p class="mt-4">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis, aspernatur quisquam quis error sapiente sed dolorem at amet voluptates.</p>
            </article>
            <article class="mt-4 ps-4"><!--ARTICULO 3-->
                <h2 class="titulo">Título del 3r artículo</h2>
                <p class="mt-4">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Corporis, aspernatur quisquam quis error sapiente sed dolorem at amet voluptates.</p>
            </article>
        </div>
    </div>
        <!--FIN DE LA PARTE CENTRAL-->

        <?php require 'panelderecho.php'; ?>

</div>        

<?php require 'pie.php'; ?>